[code]
#include "ReDrawTextUtils.iss"

type
  TBBTag = record
    Code: String;
    Param: String;
  end;

  TRow = record
    RowTop    : Integer;
    RowWidth  : Integer;
    RowHeight : Integer;
    RowItems  : Integer;
  end;

  TPart = record
    Main    : TLabel;
    Shadow  : TLabel;
    DiffX   : Integer;
    DiffY   : Integer;
  end;

  TRedrawedLabel = record
    Base      : TLabel;
    BaseStatic: TNewStaticText;
    Parts     : array of TPart;
    RowCount  : Integer;
    TotalItems: Integer;
    Left      : Integer;
    Top       : Integer;
    Width     : Integer;
    Height    : Integer;
    Rows      : array of TRow;
  end;

const
  FS_SOLID      = $8;
  FS_MINIMIZE   = $10;
  FS_LINKLABEL  = $20;
  FS_SHADOW     = $40;
  TAB_REPLACE   = '    ';

var
  TagArray: array of TBBTag;
  
procedure PushTag(const Code, Param: String);
var
  k: Integer;
begin
  k:= GetArrayLength(TagArray);
  SetArrayLength(TagArray, k + 1);
  TagArray[k].Code:= Code;
  TagArray[k].Param:= Param;
end;

function PopTag(const Code: String): Boolean;
var
  i, k, c, d: Integer;
begin
  Result:= false;
  c:= -1;
  k:= GetArrayLength(TagArray);
  
  for i:= k - 1 downto 0 do begin
    if (Lowercase(Code) = Lowercase(TagArray[i].Code)) then begin
      c:= i;
      break;
    end;
  end;
  
  if (c < 0) then
    Exit;
  
  for i:= c to k - 1 do begin
    d:= i + 1;
    if (d >= k) then
      break;
    TagArray[i].Code:= TagArray[d].Code;
    TagArray[i].Param:= TagArray[d].Param;
  end;
  
  SetArrayLength(TagArray, k - 1);
  
  Result:= true;  
end;

function TagExists(const Code: String): Integer;
var
  i: Integer;
begin
  Result:= -1;
  for i:= GetArrayLength(TagArray) - 1 downto 0 do begin
    if (Lowercase(Code) = Lowercase(TagArray[i].Code)) then begin
      Result:= i;
      Exit;
    end;
  end;
end;

procedure ClearTags();
begin
  SetArrayLength(TagArray, 0);
end;

function ParseTag(const Src: String; var Code, Param: String; var IsEndTag: Boolean): Boolean;
var
  k, l: Integer;
begin
  Result:= False;
  Code:= '';
  Param:= '';
  IsEndTag:= False;
  Src:= TrimEx(Src);
  
  if ( (Src[1] <> '[') or (Src[Length(Src)] <> ']')) then
    Exit;
    
  Src:= Copy(Src, 2, Length(Src) - 2);
  if (Src[1] = '/') then begin
    IsEndTag:= True;
    Delete(Src, 1, 1);
  end;
  
  l:= Length(Src);
  k:= Pos('=', Src);
  
  if (k > 0) then begin
    Code:= Copy(Src, 1, k - 1);
    Param:= Copy(Src, k + 1, l);
  end else begin
    Code:= Src;
  end;
   
  case Lowercase(Code) of 
    'b': ;
    'i': ;
    'u': ;
    's': ;
    'color': ;
    'min': ;
    'name': ;
    'size': ;
    'solid': ;
    'url': ;
    'shadow': ;
  else
    begin
      Code:= '';
      Param:= '';
      Exit;
    end;
  end;
  
  Result:= True;
end;  
  
procedure CopyFont(InFnt: TFont; var OutFont: TFont);
begin
  OutFont.Height:= InFnt.Height;
  OutFont.Name:= InFnt.Name;
  OutFont.Pitch:= InFnt.Pitch;
  OutFont.Size:= InFnt.Size;
  OutFont.Style:= InFnt.Style;
  OutFont.PixelsPerInch:= InFnt.PixelsPerInch;
  OutFont.Color:= InFnt.Color;
end;

function ValidTag(const Text: String; var Tag: String): Boolean;
var
  c, l, ctr: Integer;
begin
  Tag:= '';
  Result:= false;
  if (Pos('[', Text) <> 1) then
    Exit;
  c:= 2;
  ctr:= 1;
  l:= Length(Text);
  
  while (c <= l) do begin
    if (Text[c] = '[') then
      Inc(ctr);
    if (Text[c] = ']') then
      Dec(ctr);
      
    if (ctr = 0) or (ctr > 1) then
      break;
    Inc(c);
  end;
  
  if (c <= l) and (ctr = 0) then begin
    Tag:= Copy(Text, 1, c);
    Result:= true;
  end;   
end;

procedure LinkClick(Sender: TObject);
var
  Res: Integer;
begin
  ShellExec('open', TLabel(Sender).Hint, '', '', SW_SHOW, ewNoWait, Res);
end;

#ifdef IS_ENHANCED
procedure LinkMouseEnter(Sender: TObject);
begin
  TLabel(Sender).Font.Style:= TLabel(Sender).Font.Style + [fsUnderline];
end;

procedure LinkMouseLeave(Sender: TObject);
begin
  TLabel(Sender).Font.Style:= TLabel(Sender).Font.Style - [fsUnderline];
end;
#endif

procedure SetRWAlignment(var RWLbl: TRedrawedLabel; const Align: TAlignment);
var
  i, k, c, w, d: Integer;
begin
  c:= 0;
  for i:= 0 to (RWLbl.RowCount - 1) do begin
    w:= RWlbl.Rows[i].RowWidth;

    case Align of
      taLeftJustify : d:= RWLbl.Left + 0;
      taCenter      : d:= RWLbl.Left + (RWLbl.Width div 2) - (w div 2);
      taRightJustify: d:= RWLbl.Left + RwLbl.Width - w;
    end;

    for k:= c to (c + RWLbl.Rows[i].RowItems - 1) do begin
      RWLbl.Parts[k].Main.Left:= d;
      if (RWLbl.Parts[k].Shadow <> nil) then
        RWLbl.Parts[k].Shadow.Left:= d + RWLbl.Parts[k].DiffX;
      d:= d + RWLbl.Parts[k].Main.Width;
    end;

    c:= c + RWLbl.Rows[i].RowItems;
  end;
end;

procedure ShowRWLabel(var RWLbl: TRedrawedLabel);
var
  i: Integer;
begin
  for i:= 0 to RWLbl.TotalItems - 1 do begin
    if (RWLbl.Parts[i].Shadow <> nil) then
      RWLbl.Parts[i].Shadow.Show();  
    RWLbl.Parts[i].Main.Show();
  end;
end;

procedure HideRWLabel(var RWLbl: TRedrawedLabel);
var
  i: Integer;
begin
  for i:= 0 to RWLbl.TotalItems - 1 do begin
    if (RWLbl.Parts[i].Shadow <> nil) then
      RWLbl.Parts[i].Shadow.Hide();  
    RWLbl.Parts[i].Main.Hide();
  end;
end;

procedure DestroyRWLabel(var RWLbl: TRedrawedLabel);
var
  i: Integer;
begin
  for i:= 0 to RWLbl.TotalItems - 1 do begin
    RWLbl.Parts[i].Main.Free();
    if (RWLbl.Parts[i].Shadow <> nil) then
      RWLbl.Parts[i].Shadow.Free();
  end;
  SetArrayLength(RWLbl.Parts, 0);
  SetArrayLength(RWLbl.Rows, 0);
  RWLbl.Left    := 0;
  RWLbl.Top     := 0;
  RWLbl.Width   := 0;
  RWLbl.Height  := 0;
  RWlbl.RowCount:= 0;
  RWLbl.TotalItems:= 0;
  RWLbl.Base:= nil;
  RWLbl.BaseStatic:= nil;
end;

function ReDrawText(pParent: TWinControl; const pText: String; pLeft, pTop, pWidth, pHeight: Integer; pFont: TFont): TRedrawedLabel;
var
  tLeft, tTop, tWidth, tHeight, Row, itr: Integer;
  k1, k2, k3, d, dWidth, ml, dl, tl: Integer;
  defSpace, defHeight, minlen: Integer;
  tText, pTag, pCode, pParam, tText2, tUrl: String;
  newline, pEnd: Boolean;
  tmpFont: TFont;
  tFlags: Longword;
  ShadowColor: TColor;
  sdDiffX, sdDiffY: Integer;
  params: array of String;
begin
  DestroyRWLabel(Result);
  
  if (pText = '') then
    Exit;

  if (pFont = nil) then
    pFont:= WizardForm.Font;


  StringChange(pText, #9, TAB_REPLACE);
  StringChange(pText, '\t', TAB_REPLACE);

  tLeft   := pLeft;
  tTop    := pTop;
  tText   := '';
  newline := false;
  Row     := 0;
  itr     := 0;

  Result.Left:= pLeft;
  Result.Top:= pTop;
  Result.Width:= pWidth;
  Result.Height:= pHeight;
  Result.TotalItems:= 0;
  Result.RowCount:= 1;
  SetArrayLength(Result.Rows, Result.RowCount);
  Result.Rows[Row].RowTop:= pTop;

  ClearTags();

  tmpFont:= TFont.Create();

  defSpace:= GetTextWidth(pFont, ' ');
  defHeight:= GetTextHeight(pFont, ' ') + 2;

  while (pText <> '') or (tText <> '') do begin
    k1:= TagExists('url');
    if (k1 >= 0) and (tText = '') and (Pos('[', pText) = 1) then
      tText:= TagArray[k1].Param;

    if (tText <> '') and (not newline) then begin
      CopyFont(pFont, tmpFont);
      tText2:= '';
      tUrl:= '';
      minlen:= 0;
      tFlags:= 0;
      dWidth:= pWidth - tLeft;

      for d:= 0 to GetArrayLength(TagArray) - 1 do begin
        case Lowercase(TagArray[d].Code) of
          'b'    :  tmpFont.Style:= tmpFont.Style + [fsBold];
          'i'    :  tmpFont.Style:= tmpFont.Style + [fsItalic];
          'u'    :  tmpFont.Style:= tmpFont.Style + [fsUnderline];
          's'    :  tmpFont.Style:= tmpFont.Style + [fsStrikeOut];
          'color':  tmpFont.Color:= StrToInt(TagArray[d].Param);
          'min'  :  begin
                      minlen:= StrToInt(TagArray[d].Param);
                      tFlags:= tFlags or FS_MINIMIZE or FS_SOLID;
                    end;
          'name' :  tmpFont.Name:= TagArray[d].Param;
          'size' :  tmpFont.Size:= StrToInt(TagArray[d].Param);
          'solid':  tFlags:= tFlags or FS_SOLID;
          'url'  :  begin
                      tUrl:= TagArray[d].Param;
                      tFlags:= tFlags or FS_LINKLABEL;
                    end;
          'shadow': begin
                      params:= SplitString(TagArray[d].Param, ',');
                      ShadowColor:= 0;
                      sdDiffX:= 1;
                      sdDiffY:= 1;
                      case GetArrayLength(params) of
                        1:  begin
                              ShadowColor:= StrToInt(params[0]);
                            end;
                        2:  begin
                              ShadowColor:= StrToInt(params[0]);
                              sdDiffX:= StrToInt(params[1]);
                              sdDiffY:= sdDiffX;
                            end;
                        3:  begin
                              ShadowColor:= StrToInt(params[0]);
                              sdDiffX:= StrToInt(params[1]);
                              sdDiffY:= StrToInt(params[2]);
                            end;
                      end;
                      SetArrayLength(params, 0);
                      tFlags:= tFlags or FS_SHADOW;
                    end;
        end;
      end;

      if (tFlags and FS_SOLID <> 0) then begin
        tl:= Length(tText);
      end else begin
        ml:= GetTextLen(tmpFont, tText, dWidth);
        if (ml <= 0) then begin
          newline:= true;
          Continue;
        end;
        tl:= ml;

        if (tl > Length(tText)) then
          tl:= Length(tText);

        dl:= FindSpace(tText, 0, tl);
        if (dl > 0) and (dl < tl) and (tl <> Length(tText)) then
          tl:= dl;
      end;

      tText2:= Copy(tText, 1, tl);

      if (tFlags and FS_MINIMIZE <> 0) then
        tText2:= MinimizePathName(tText2, tmpFont, minlen);

      if (tText2 <> '') then begin
        tWidth:= GetTextWidth(tmpFont, tText2);
        tHeight:= GetTextHeight(tmpFont, tText2) + 2;

        if (tLeft <> pLeft) and ((tLeft + tWidth) - (pLeft + pWidth) >= 15) then begin
          tText:= TrimLeft(tText);
          newline:= true;
          Continue;
        end;

        d:= GetArrayLength(Result.Parts);
        SetArrayLength(Result.Parts, d + 1);

        Result.Rows[Row].RowWidth:= Result.Rows[Row].RowWidth + tWidth;
        if (Result.Rows[Row].RowHeight < tHeight) then
          Result.Rows[Row].RowHeight:= tHeight;
        Inc(Result.Rows[Row].RowItems);
        Inc(Result.TotalItems);

        if (tFlags and FS_SHADOW <> 0) then begin
          Result.Parts[d].Shadow:= TLabel.Create(pParent);
          Result.Parts[d].Shadow.Autosize:= false;
          Result.Parts[d].Shadow.Font:= tmpFont;
          Result.Parts[d].Shadow.Font.Color:= ShadowColor;
          Result.Parts[d].Shadow.SetBounds(tLeft + sdDiffX, tTop + sdDiffY, tWidth, tHeight);
          Result.Parts[d].Shadow.Transparent:= true;
          Result.Parts[d].Shadow.Caption:= tText2;
          Result.Parts[d].Shadow.Parent:= pParent;
          Result.Parts[d].DiffX:= sdDiffX;
          Result.Parts[d].DiffY:= sdDiffY;
        end;
        
        Result.Parts[d].Main:= Tlabel.Create(pParent);
        Result.Parts[d].Main.Autosize:= false;
        Result.Parts[d].Main.Font:= tmpFont;
        Result.Parts[d].Main.SetBounds(tLeft, tTop, tWidth, tHeight);
        Result.Parts[d].Main.Transparent:= true;
        Result.Parts[d].Main.Caption:= tText2;
        if (tFlags and FS_LINKLABEL <> 0) then begin
          Result.Parts[d].Main.Hint:= tUrl;
          #ifdef IS_ENHANCED
          Result.Parts[d].Main.OnMouseEnter:= @LinkMouseEnter;
          Result.Parts[d].Main.OnMouseLeave:= @LinkMouseLeave;
          #endif
          Result.Parts[d].Main.OnClick:= @LinkClick;
          Result.Parts[d].Main.Cursor:= crHand;
        end;
        Result.Parts[d].Main.Parent:= pParent;

        tLeft:= tLeft + tWidth;
        if (tFlags and FS_MINIMIZE = 0) then
          Delete(tText, 1, Length(tText2))
        else
          tText:= '';
      end;

      if (tText <> '') then begin
        tText:= TrimLeft(tText);
        newline:= true;
        Continue;
      end;
    end;
    
    if (Result.Rows[Row].RowHeight = 0) then
      Result.Rows[Row].RowHeight:= defHeight;

    k1:= Pos('\n', pText);
    k2:= Pos(#13#10, pText);
    if (k1 = 1) or (k2 = 1) then begin
      Delete(pText, 1, 2);
      newline:= true;    
    end;   

    if (newline) then begin
      pText:= TrimLeft(pText);
      Inc(Row);
      Inc(Result.RowCount);
      SetArrayLength(Result.Rows, Result.RowCount);
      Result.Rows[Row].RowTop:= 0;
      Result.Rows[Row].RowItems:= 0;
      Result.Rows[Row].RowWidth:= 0;
      Result.Rows[Row].RowHeight:= 0;
      if (Row > 0) then
        Result.Rows[Row].RowTop:= Result.Rows[Row - 1].RowTop + Result.Rows[Row - 1].RowHeight;
      tLeft:= pLeft;
      tTop:= Result.Rows[Row].RowTop;

      newline:= false;
      Continue;
    end;
    
    k3:= Pos('[', pText);
    if (k3 = 1) then begin
      if ( ValidTag(pText, pTag) ) then begin
        if ( ParseTag(pTag, pCode, pParam, pEnd) ) then begin
          if (pEnd) then
            PopTag(pCode)
          else
            PushTag(pCode, pParam);
          Delete(pText, 1, Length(pTag));
        end else begin
          tText:= tText + pTag;
          Delete(pText, 1, Length(pTag));
        end;
      end else begin
        tText:= tText + pText[1];
        Delete(pText, 1, 1);
      end;
    end else begin
      if (k3 <= 0) then
        k3:= Length(pText) + 1;
      if (k2 > 0) and (k2 < k3) then
        k3:= k2;
      if (k1 > 0) and (k1 < k3) then
        k3:= k1;

      tText:= tText + Copy(pText, 1, k3 - 1);
      Delete(pText, 1, k3 - 1);
    end;
  end;

  k3:= 0;
  for k1:= 0 to Result.RowCount - 1 do begin
    for k2:= k3 to k3 + Result.Rows[k1].RowItems - 1 do begin
      dl:= Result.Rows[k1].RowHeight - Result.Parts[k2].Main.Height;
      Result.Parts[k2].Main.Top:= Result.Rows[k1].RowTop + dl;
      if (Result.Parts[k2].Shadow <> nil) then
        Result.Parts[k2].Shadow.Top:= Result.Rows[k1].RowTop + dl + Result.Parts[k2].DiffY;
    end;

    k3:= k3 + Result.Rows[k1].RowItems;
  end;

  tmpFont.Free();
  ClearTags();
end;

function ReDrawLabel(Lbl: TLabel): TRedrawedLabel;
begin
  Result:= ReDrawText(Lbl.Parent, lbl.Caption, Lbl.Left, Lbl.Top, Lbl.Width, Lbl.Height, Lbl.Font);
  SetRWAlignment(Result, Lbl.Alignment);
  Result.Base:= Lbl;
  Lbl.Hide;
end;

function ReDrawStatic(Lbl: TNewStaticText): TRedrawedLabel;
begin
  Result:= ReDrawText(Lbl.Parent, lbl.Caption, Lbl.Left, Lbl.Top, Lbl.Width, Lbl.Height, Lbl.Font);
  Result.BaseStatic:= Lbl;
  Lbl.Hide;
end;
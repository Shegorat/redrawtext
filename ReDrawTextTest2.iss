[Setup]
AppName=MyApp
AppVername=MyApp
DefaultDirName={pf}\MyApp

[code]
#include "ReDrawText.iss"

function GetTickCount: DWord; external 'GetTickCount@kernel32';

var myLbl: TLabel;
    MyLblRdw: TRedrawedLabel;
    Ticks: DWORD;

procedure EditOnChange(Sender: TObject);
begin
  Mylbl.Caption:= '���� ���� ����� ����������� � [b][min=200][color=$FF0000]'+WizardForm.DirEdit.Text+'[/color][/b][/min]' +
  ' � ��� ��������� ����������� [shadow=$000000][i][color=$FF0000]9.98 ��[/color][/i][/shadow] ���������� ��������� ������������.'+
  '\n��� ����� ��������� ���������� ��������� �� ���� [url=https://krinkels.org]����� Krinkels.org[/url] ...';
  ticks:= GetTickCount();
  MyLblRdw:= ReDrawLabel(MyLbl);
  log('Drawing time '+IntToStr(GetTickCount-ticks)+'ms.')
end;

procedure initializeWizard();
begin
  Mylbl:= TLabel.Create(WizardForm);
  MyLbl.AutoSize:= False;
  MyLbl.WordWrap:= true;
  MyLbl.SetBounds(0, 130, 410, 80);
  MyLbl.Parent:= WizardForm.SelectDirPage;
  MyLbl.Alignment:= taCenter;
  
  WizardForm.DirEdit.OnChange:= @EditOnChange;
  WizardForm.DirEdit.Text:= WizardForm.DirEdit.Text+#0;
end;
[code]
  #define API_ENCODING defined(UNICODE) ? "W" : "A"

type
  TSize = record
    cx: Longint;
    cy: Longint;
  end;

  ABC = record
    abcA: Integer;
    abcB: UINT;
    abcC: Integer;
  end;

  HDC     = Longword;
  HGDIOBJ = Longword;

function GetTextExtentPoint32(DC: HDC; lpString: String; Len: Integer; var lpSize: TSize): Boolean; external 'GetTextExtentPoint32{#API_ENCODING}@gdi32.dll stdcall';
function GetTextExtentExPoint(DC: HDC; lpszStr: String; cchString, nMaxExtent: Integer; var lpnFit: Integer; alpDx: Integer; var lpSize: TSize): Boolean; external 'GetTextExtentExPoint{#API_ENCODING}@gdi32.dll stdcall';
function GetClientDC(hWnd: HWND): HDC; external 'GetDC@user32.dll stdcall';
function ReleaseClientDC(hWnd: HWND; hDC: HDC): Integer; external 'ReleaseDC@user32.dll stdcall';
function SelectObject(DC: HDC; p2: HGDIOBJ): HGDIOBJ; external 'SelectObject@gdi32.dll stdcall';
function SetRect(var lpRect: TRect; xLeft,yTop,xRight,yBottom: Integer): Boolean; external 'SetRect@user32.dll stdcall';
function InvalidateRect(hWnd: HWND; lpRect: TRect; bErase: BOOL): BOOL; external 'InvalidateRect@user32.dll stdcall';
function GetCharABCWidths(DC: HDC; uFirstChar, uLastChar: UINT; var lpAbc: ABC): Boolean; external 'GetCharABCWidths{#API_ENCODING}@gdi32.dll stdcall';


function Inc(var C: Integer): Integer;
begin
  C:= C + 1;
  Result:= C;
end;

function Dec(var C: Integer): Integer;
begin
  C:= C - 1;
  Result:= C;
end;

function FindSpace(s: string; Min, Max: Integer): Integer;
var
  n: integer;
begin
  Result:= -1;
  if (Max = 0) or (s = '') then Exit;
  if (Max > Length(S)) then
    Max:=Length(S);
  if (Min <= 0) then
    Min:= 1;

  if (s[Max] = ' ') then begin
    Result:= Max;
    Exit;
  end;

  for n:= Max downto min do begin
    if (s[n] = ' ') then begin
      Result:= n;
      Break;
    end;
  end;
end;

function GetTextWidth(aFont: TFont; Text: String): Integer;
var
  DC: HDC;
  aSize: TSize;
  OldObj: Longword;
  abc1, abc2: ABC;
begin
  Result:= 0;

  if (Length(Text) = 0) then
    Exit;

  DC:= GetClientDC(0);
  try
    OldObj:= SelectObject(DC, aFont.Handle);
    GetTextExtentPoint32(DC, Text, Length(Text), aSize);
    Result:= aSize.cx;
    if ( (Length(Text) <> 1) and (fsItalic in aFont.Style) ) then begin
      GetCharABCWidths(DC, Ord(Text[1]), Ord(Text[1]), abc1);
      GetCharABCWidths(DC, Ord(Text[Length(text)]), Ord(Text[Length(text)]), abc2);

      if (abc1.abcA < 0) then
        Result:= Result-abc1.abcA;

      if (abc2.abcC < 0) then
        Result:= Result-abc2.abcC;

    end;
  finally
    SelectObject(DC, OldObj);
    ReleaseClientDC(0, DC);
  end;
end;

function GetTextHeight(aFont: TFont; Text: String): Integer;
var
  DC: HDC;
  aSize: TSize;
  OldObj: Longword;
begin
  Result:= 0;
  if (Length(Text) = 0) then
    Exit;

  DC:= GetClientDC(0);
  try
    OldObj:= SelectObject(DC, aFont.Handle);
    GetTextExtentPoint32(DC, Text, Length(Text), aSize);
    Result:= aSize.cy;
  finally
    SelectObject(DC, OldObj);
    ReleaseClientDC(0, DC);
  end;
end;

function GetTextLen(aFont: TFont; Text: String; MaxWidth: Integer): Integer;
var
  DC: HDC;
  aSize: TSize;
  OutLen: Integer;
  OldObj: Longword;
begin
  Result:= 0;

  if (Length(Text) = 0) then
    Exit;

  DC:= GetClientDC(0);
  try
    OldObj:= SelectObject(DC, aFont.Handle);
    GetTextExtentExPoint(DC, Text, Length(Text), MaxWidth, OutLen, 0, aSize);
    Result:= OutLen;
  finally
    SelectObject(DC, OldObj);
    ReleaseClientDC(0, DC);
  end;
end;


function SplitString(S: String; Delim: String): array of String;
var
  i, k: Integer;
begin
  SetArrayLength(Result, 0);

  while (S <> '') do begin
    k:= Pos(Delim, S);
    if (k <= 0) then
      k:= Length(S) + 1;

    i:= GetArrayLength(Result);
    SetArrayLength(Result, i + 1);
    Result[i]:= Trim(Copy(S, 1, k - 1));
    Delete(S, 1, k - 1 + Length(Delim));
    S:= Trim(S);
  end;
end;

function TrimEx(S: String): String;
begin
  if (S <> '') then begin
    while (Length(S) > 0) and (S[1] = ' ') do
      Delete(S, 1, 1);

    while (Length(S) > 0) and (S[length(S)] = ' ') do
      Delete(S, Length(S), 1);
  end;
  Result:= S;
end;